Download etcd & etcdctl
```
 mkdir -p ~/k8s/tmp
 mkdir -p ~/k8s/work
 mkdir -p ~/k8s/bin
 cd ~/k8s/tmp && wget https://github.com/coreos/etcd/releases/download/v3.3.10/etcd-v3.3.10-linux-amd64.tar.gz
 tar -xvf etcd-v3.3.10-linux-amd64.tar.gz
 mv ~/k8s/tmp/etcd-v3.3.10-linux-amd64/etcd* ~/k8s/bin/
 export PATH=$HOME/k8s/bin:$PATH
```
```
cat >etcd.service <<EOF
[Unit]
Description=Etcd Server
After=network.target
After=network-online.target
Wants=network-online.target
Documentation=https://github.com/coreos

[Service]
Type=notify
WorkingDirectory=/data/k8s/etcd/data
ExecStart=/home/user/k8s/bin/etcd \\
  --data-dir=/data/k8s/etcd/data \\
  --wal-dir=/data/k8s/etcd/wal \\
  --name=etcd-2 \\
  --cert-file=/etc/etcd/cert/etcd.pem \\
  --key-file=/etc/etcd/cert/etcd-key.pem \\
  --trusted-ca-file=/etc/kubernetes/cert/ca.pem \\
  --peer-cert-file=/etc/etcd/cert/etcd.pem \\
  --peer-key-file=/etc/etcd/cert/etcd-key.pem \\
  --peer-trusted-ca-file=/etc/kubernetes/cert/ca.pem \\
  --peer-client-cert-auth \\
  --client-cert-auth \\
  --listen-peer-urls=https://10.170.0.3:2380 \\
  --initial-advertise-peer-urls=https://10.170.0.3:2380 \\
  --listen-client-urls=https://10.170.0.3:2379,http://127.0.0.1:2379 \\
  --advertise-client-urls=https://10.170.0.3:2379 \\
  --initial-cluster-token=etcd-cluster-0 \\
  --initial-cluster=etcd-1=https://10.170.0.2:2380,etcd-2=https://10.170.0.3:2380,etcd-3=https://10.170.0.4:2380 \\
  --initial-cluster-state=new \\
  --auto-compaction-mode=periodic \\
  --auto-compaction-retention=1 \\
  --max-request-bytes=33554432 \\
  --quota-backend-bytes=6442450944 \\
  --heartbeat-interval=250 \\
  --election-timeout=2000
Restart=on-failure
RestartSec=5
LimitNOFILE=65536
[Install]
WantedBy=multi-user.target
EOF

sudo mkdir -p /data/k8s/etcd/data
sudo mkdir -p /data/k8s/etcd/wal

sudo firewall-cmd --add-port=2379/tcp --zone=public --permanent
sudo firewall-cmd --add-port=2380/tcp --zone=public --permanent
sudo firewall-cmd --reload


sudo mv etcd.service /etc/systemd/system/etcd.service
sudo systemctl daemon-reload && sudo systemctl enable etcd && sudo systemctl restart etcd
sudo systemctl status etcd.service



export PATH=$HOME/k8s/bin:$PATH
ETCDCTL_API=3 etcdctl member list

```


